<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkan laskeminen</title>
</head>
<body>
    <?php
        $brutto = filter_input(INPUT_POST,'brutto');
        $ennakko = filter_input(INPUT_POST,'ennakko');
        $elake = filter_input(INPUT_POST,'elake',);
        $vakuutus = filter_input(INPUT_POST,'vakuutus',);

        $lasennakko = ($brutto/100)*$ennakko;
        $laselake = ($brutto/100)*$elake;
        $lasvakuutus = ($brutto/100)*$vakuutus;
        $netto = $brutto - $lasvakuutus - $laselake - $lasvakuutus;


        printf("<p>Ennakonpidätys on %.d euroa. </p>",$lasennakko);
        printf("<p>Työeläkemaksu on %.d euroa</p>",$laselake);
        printf("<p>Työttömyysvakuutusmaksu on %.d euroa </p>",$lasvakuutus);

        printf("<h3>Nettopalkkasi on %.d euroa </h3>",$netto);
    ?>
    
</body>
</html>